**EJD Construction**

We specialize in moving block walls, bringing down load-bearing walls, up-turn beams, moving structural components, and relocating bearing points. There is no remodeling aspect in an existing home that is too complicated for us. At EJD Construction we have more than 20 years in residential construction, which enable us to make your home remodeling dreams come true at a reasonable cost. We work well with Architects in assisting with design concepts and their economic impact.
 [https://www.ejdconstruction.com/home-remodeling/](https://www.ejdconstruction.com/home-remodeling/) 
---

